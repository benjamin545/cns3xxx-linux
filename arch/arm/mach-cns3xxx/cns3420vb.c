/*
 * Cavium Networks CNS3420 Validation Board
 *
 * Copyright 2000 Deep Blue Solutions Ltd
 * Copyright 2008 ARM Limited
 * Copyright 2008 Cavium Networks
 *		  Scott Shu
 * Copyright 2010 MontaVista Software, LLC.
 *		  Anton Vorontsov <avorontsov@mvista.com>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, Version 2, as
 * published by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/compiler.h>
#include <linux/io.h>
#include <linux/dma-mapping.h>
#include <linux/serial_core.h>
#include <linux/serial_8250.h>
#include <linux/platform_device.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/physmap.h>
#include <linux/mtd/partitions.h>
#include <linux/i2c.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/clk-provider.h>
#include <linux/clkdev.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/time.h>
#include "cns3xxx.h"
#include "pm.h"
#include "core.h"
#include "devices.h"

/*
 * UART
 */
static void __init cns3420_early_serial_setup(void)
{
#ifdef CONFIG_SERIAL_8250_CONSOLE
	static struct uart_port cns3420_serial_port = {
		.membase        = (void __iomem *)CNS3XXX_UART0_BASE_VIRT,
		.mapbase        = CNS3XXX_UART0_BASE,
		.irq            = IRQ_CNS3XXX_UART0,
		.iotype         = UPIO_MEM,
		.flags          = UPF_BOOT_AUTOCONF | UPF_FIXED_TYPE,
		.regshift       = 2,
		.uartclk        = 24000000,
		.line           = 0,
		.type           = PORT_16550A,
		.fifosize       = 16,
	};

	early_serial_setup(&cns3420_serial_port);
#endif
}

/*
 * I2C
 */
static struct resource cns3420_i2c_resource[] = {
	{
		.start    = CNS3XXX_SSP_BASE + 0x20,
		.end      = CNS3XXX_SSP_BASE + 0x3f,
		.flags    = IORESOURCE_MEM,
	}, {
		.start    = IRQ_CNS3XXX_I2C,
		.flags    = IORESOURCE_IRQ,
	},
};

static struct platform_device cns3420_i2c_controller = {
	.name   = "cns3xxx-i2c",
	.num_resources  = ARRAY_SIZE(cns3420_i2c_resource),
	.resource = cns3420_i2c_resource,
};

static struct i2c_board_info cns3420_i2c_devices[] __initdata = {

#if defined(CONFIG_SENSORS_ADT7475)
    {
        I2C_BOARD_INFO("adt7473", 0x2e),
    },
#endif

};

/*
 * SPI
 */
static struct mtd_partition cns3420_spi_partitions[] = {
	{
		.name		= "SPI-UBoot",
		.size		= 0x30000,
		.offset		= 0,
		.mask_flags	= MTD_WRITEABLE,
	}, {
		.name		= "SPI-UBootEnv",
		.size		= 0x10000,
		.offset		= MTDPART_OFS_APPEND,
	}, {
		.name		= "SPI-FileSystem",
		.size		= 0x40000,
		.offset		= MTDPART_OFS_APPEND,
	},
};

static struct flash_platform_data cns3420_spi_pdata = {
	.parts = cns3420_spi_partitions,
	.nr_parts = ARRAY_SIZE(cns3420_spi_partitions),
};

static struct spi_board_info __initdata cns3420_spi_devices[] = {
	{
		.modalias = "m25p80",
		.platform_data = &cns3420_spi_pdata,
		.max_speed_hz = 80000000,
		.chip_select = 0,
        .mode = SPI_RX_DUAL,
	},
};

static struct resource cns3420_spi_resource[] = {
    {
	    .start    = CNS3XXX_SSP_BASE + 0x40,
	    .end      = CNS3XXX_SSP_BASE + 0x6f,
	    .flags    = IORESOURCE_MEM,
    }, {
		.start    = IRQ_CNS3XXX_SPI,
		.flags    = IORESOURCE_IRQ,
	},
};

static struct platform_device cns3420_spi_controller = {
	.name = "cns3xxx_spi",
	.num_resources = ARRAY_SIZE(cns3420_spi_resource),
	.resource = cns3420_spi_resource,
};

/* 
 * RTC 
 */
static struct resource cns3420_rtc_resources[] = {
	{
		.start = CNS3XXX_RTC_BASE,
//		.end   = CNS3XXX_RTC_BASE + PAGE_SIZE - 1,
        .end   = CNS3XXX_RTC_BASE + 0x38,
		.flags = IORESOURCE_MEM,
	}, {
		.start = IRQ_CNS3XXX_RTC,
		.flags = IORESOURCE_IRQ,
	}
};

static struct platform_device cns3420_rtc_controller = {
	.name	   = "cns3xxx-rtc",
	.id	     = -1,
	.num_resources  = ARRAY_SIZE(cns3420_rtc_resources),
	.resource       = cns3420_rtc_resources,
};

/*
 * Initialization
 */
static struct platform_device *cns3420_pdevs[] __initdata = {
    &cns3420_rtc_controller,
	&cns3420_i2c_controller,
	&cns3420_spi_controller,
};

static void __init cns3420_init(void)
{
	clk_register_clkdev(clk_register_fixed_rate(NULL, "cpu", NULL, CLK_IGNORE_UNUSED,
				      cns3xxx_cpu_clock() * (1000000)), "cpu", NULL);

	clk_register_clkdev(clk_register_fixed_rate(NULL, "aclk", NULL, CLK_IGNORE_UNUSED,
				      cns3xxx_cpu_clock() * (1000000 / 2)), "aclk", NULL);

	clk_register_clkdev(clk_register_fixed_rate(NULL, "hclk", NULL, CLK_IGNORE_UNUSED,
				      cns3xxx_cpu_clock() * (1000000 / 4)), "hclk", NULL);

    clk_register_clkdev(clk_register_fixed_rate(NULL, "dclk", NULL, CLK_IGNORE_UNUSED,
				      cns3xxx_cpu_clock() * (1000000 / 6)), "dclk", NULL);

	clk_register_clkdev(clk_register_fixed_rate(NULL, "pclk", NULL, CLK_IGNORE_UNUSED,
				      cns3xxx_cpu_clock() * (1000000 / 8)), "pclk", NULL);

	cns3xxx_l2x0_init();

	platform_add_devices(cns3420_pdevs, ARRAY_SIZE(cns3420_pdevs));

	cns3xxx_ahci_init();
	cns3xxx_sdhci_init();

	i2c_register_board_info(0, cns3420_i2c_devices,
			ARRAY_SIZE(cns3420_i2c_devices));
	spi_register_board_info(cns3420_spi_devices,
			ARRAY_SIZE(cns3420_spi_devices));

	pm_power_off = cns3xxx_power_off;
}

static struct map_desc cns3420_io_desc[] __initdata = {
	{
		.virtual	= CNS3XXX_UART0_BASE_VIRT,
		.pfn		= __phys_to_pfn(CNS3XXX_UART0_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	},
};

static void __init cns3420_map_io(void)
{
	cns3xxx_map_io();
	iotable_init(cns3420_io_desc, ARRAY_SIZE(cns3420_io_desc));

	cns3420_early_serial_setup();
}

MACHINE_START(CNS3420VB, "Cavium Networks CNS3420 Validation Board")
	.smp 		= smp_ops(cns3xxx_smp_ops),
	.atag_offset	= 0x100,
	.map_io		= cns3420_map_io,
	.init_irq	= cns3xxx_init_irq,
	.init_time	= cns3xxx_timer_init,
	.init_machine	= cns3420_init,
	.init_late      = cns3xxx_pcie_init_late,
	.restart	= cns3xxx_restart,
MACHINE_END

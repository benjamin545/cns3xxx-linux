/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */

#include <linux/init.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/module.h>

#define SPI_CONFIGURATION_REG(p)			(u32 volatile *)(p->base + 0x00)
#define SPI_SERVICE_STATUS_REG(p)			(u32 volatile *)(p->base + 0x04)
#define SPI_BIT_RATE_CONTROL_REG(p)		    (u32 volatile *)(p->base + 0x08)
#define SPI_TRANSMIT_CONTROL_REG(p)		    (u32 volatile *)(p->base + 0x0C)
#define SPI_TRANSMIT_BUFFER_REG(p)		    (u32 volatile *)(p->base + 0x10)
#define SPI_RECEIVE_CONTROL_REG(p)			(u32 volatile *)(p->base + 0x14)
#define SPI_RECEIVE_BUFFER_REG(p)			(u32 volatile *)(p->base + 0x18)
#define SPI_FIFO_TRANSMIT_CONFIG_REG(p)     (u32 volatile *)(p->base + 0x1C)
#define SPI_FIFO_TRANSMIT_CONTROL_REG(p)	(u32 volatile *)(p->base + 0x20)
#define SPI_FIFO_RECEIVE_CONFIG_REG(p)		(u32 volatile *)(p->base + 0x24)
#define SPI_INTERRUPT_STATUS_REG(p)		    (u32 volatile *)(p->base + 0x28)
#define SPI_INTERRUPT_ENABLE_REG(p)		    (u32 volatile *)(p->base + 0x2C)


/* SPI_CONFIGURATION_REG */
union cns3xxx_spi_cfg {
	uint32_t u32;
	struct cns3xxx_spi_cfg_s {
#ifdef __BIG_ENDIAN_BITFIELD
        uint32_t enable:1;
		uint32_t hibootspd:1;
		uint32_t twoioread:1;
		uint32_t reserved_25_28:4;
		uint32_t dataswap:1;
		uint32_t reserved_15_23:9;
		uint32_t clkpol:1;
		uint32_t clkphase:1;
		uint32_t loopback:1;
		uint32_t mstrmode:1;
		uint32_t fifoen:1;
		uint32_t spimode:1;
		uint32_t reserved_2_8:7;
        uint32_t shiftlength:2;
#else  /* __BIG_ENDIAN_BITFIELD */
        uint32_t shiftlength:2;
		uint32_t reserved_2_8:7;
		uint32_t spimode:1;
	    uint32_t fifoen:1;
		uint32_t mstrmode:1;
		uint32_t loopback:1;
		uint32_t clkphase:1;
		uint32_t clkpol:1;
		uint32_t reserved_15_23:9;
		uint32_t dataswap:1;
		uint32_t reserved_25_28:4;
		uint32_t twoioread:1;
		uint32_t hibootspd:1;
        uint32_t enable:1;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_SERVICE_STATUS_REG */
union cns3xxx_service_status {
	uint32_t u32;
	struct cns3xxx_service_status_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_1_31:31;
		uint32_t busy:1;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t busy:1;
		uint32_t reserved_1_31:31;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_BIT_RATE_CONTROL_REG */
union cns3xxx_bitrate_control {
	uint32_t u32;
	struct cns3xxx_bitrate_control_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_3_31:29;
		uint32_t clkdiv:3;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t clkdiv:3;
		uint32_t reserved_3_31:29;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_TRANSMIT_CONTROL_REG */
union cns3xxx_transmit_control {
	uint32_t u32;
	struct cns3xxx_transmit_control_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_3_31:29;
		uint32_t eof:1;
		uint32_t channel:2;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t channel:2;
		uint32_t eof:1;
		uint32_t reserved_3_31:29;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_RECEIVE_CONTROL_REG */
union cns3xxx_receive_control {
	uint32_t u32;
	struct cns3xxx_receive_control_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_3_31:29;
		uint32_t eof:1;
		uint32_t channel:2;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t channel:2;
		uint32_t eof:1;
		uint32_t reserved_3_31:29;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_FIFO_TRANSMIT_CONFIG_REG */
union cns3xxx_fifo_transmit_cfg {
	uint32_t u32;
	struct cns3xxx_fifo_transmit_cfg_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_6_31:26;
		uint32_t threshold:2;
		uint32_t reserved_0_3:4;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t reserved_0_3:4;
		uint32_t threshold:2;
		uint32_t reserved_6_31:26;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_FIFO_RECEIVE_CONFIG_REG */
union cns3xxx_fifo_receive_cfg {
	uint32_t u32;
	struct cns3xxx_fifo_receive_cfg_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_6_31:26;
		uint32_t threshold:2;
		uint32_t reserved_0_3:4;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t reserved_0_3:4;
		uint32_t threshold:2;
		uint32_t reserved_6_31:26;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_INTERRUPT_STATUS_REG */
union cns3xxx_interrupt_status {
	uint32_t u32;
	struct cns3xxx_interrupt_status_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_8_31:24;
		uint32_t txunderrun:1;
		uint32_t rxoverrun:1;
		uint32_t reserved_4_5:2;
		uint32_t txempty:1;
		uint32_t rxfull:1;
		uint32_t reserved_0_1:2;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t reserved_0_1:2;
		uint32_t rxfull:1;
		uint32_t txempty:1;
		uint32_t reserved_4_5:2;
		uint32_t rxoverrun:1;
		uint32_t txunderrun:1;
		uint32_t reserved_8_31:24;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* SPI_INTERRUPT_ENABLE_REG */
union cns3xxx_interrupt_enable {
	uint32_t u32;
	struct cns3xxx_interrupt_enable_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_8_31:24;
		uint32_t txunderirqen:1;
		uint32_t rxoverirqen:1;
		uint32_t reserved_4_5:2;
		uint32_t txirqen:1;
		uint32_t rxirqen:1;
		uint32_t reserved_0_1:2;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t reserved_0_1:2;
		uint32_t rxirqen:1;
		uint32_t txirqen:1;
		uint32_t reserved_4_5:2;
		uint32_t rxoverirqen:1;
		uint32_t txunderirqen:1;
		uint32_t reserved_8_31:24;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

struct cns3xxx_spi {
	struct spi_controller *ctlr;
	struct device *dev;
    struct clk *clk;

    int irq;

	int len;

	const unsigned char *tx;
	unsigned char *rx;

	void __iomem *base;
};

static void cns3xxx_tx_wait_ready(struct cns3xxx_spi *p)
{
    union cns3xxx_interrupt_status istat;
	union cns3xxx_service_status sstat;

	do {
		sstat.u32 = readl(SPI_SERVICE_STATUS_REG(p));
	} while (sstat.s.busy);

    do {
        istat.u32 = readl(SPI_INTERRUPT_STATUS_REG(p));
    } while (!istat.s.txempty);
}

static void cns3xxx_rx_wait_ready(struct cns3xxx_spi *p)
{
	union cns3xxx_interrupt_status istat;

    do {
        istat.u32 = readl(SPI_INTERRUPT_STATUS_REG(p));
    } while (!istat.s.rxfull);
}

static void cns3xxx_set_cs(struct spi_device *spi, bool enable)
{
    union cns3xxx_spi_cfg cfg;
    union cns3xxx_transmit_control tcon;
    union cns3xxx_bitrate_control bcon;

	struct cns3xxx_spi *p;

    unsigned long clock;

    p = spi_master_get_devdata(spi->master);

    clock = (p->clk ? clk_get_rate(p->clk) : 0);

    /* 
     * set spi clock to highest speed under spi->max_speed_hz
     * clkdiv 0-3 is linear at       1/2,  1/4,  1/6,  1/8
     * then grows exponentially      1/16, 1/32, 1/64, 1/128
     */
    bcon.u32 = readl( SPI_BIT_RATE_CONTROL_REG(p));
    if (clock && spi->max_speed_hz && (clock / spi->max_speed_hz < 128)) {
        unsigned int div = ((clock / spi->max_speed_hz) / 2);
        if (div > 3) {
            div = 4;
            while ( div < 8 && (clock / spi->max_speed_hz < (1 << div)))
                div++;
        }
        bcon.s.clkdiv = div;
    }
    else
        bcon.s.clkdiv = 0x7;
    writel(bcon.u32, SPI_BIT_RATE_CONTROL_REG(p));

    cfg.u32 = readl(SPI_CONFIGURATION_REG(p));
    cfg.s.clkphase      = (spi->mode & SPI_CPHA ?         1 : 0);
    cfg.s.clkpol        = (spi->mode & SPI_CPOL ?         1 : 0);
    cfg.s.twoioread     = (spi->mode & SPI_RX_DUAL ?      1 : 0);
    cfg.s.dataswap      = (spi->mode & SPI_LSB_FIRST ?    1 : 0);
    cfg.s.loopback      = (spi->mode & SPI_LOOP ?         1 : 0);
    writel(cfg.u32, SPI_CONFIGURATION_REG(p));

    tcon.u32 = readl(SPI_TRANSMIT_CONTROL_REG(p));
    tcon.s.channel = (enable ? 0 : spi->chip_select);
    writel(tcon.u32, SPI_TRANSMIT_CONTROL_REG(p));
}

static int cns3xxx_transfer_one(struct spi_controller *ctlr, 
                         struct spi_device *spi, 
                         struct spi_transfer *t,
				         bool last_xfer)
{
    union cns3xxx_transmit_control tcon;

    struct cns3xxx_spi *p;

    unsigned int i;

    p = spi_master_get_devdata(ctlr);

	p->tx = t->tx_buf;
	p->rx = t->rx_buf;
	p->len = t->len;

	for (i = 0; i < p->len; i++) {
        cns3xxx_tx_wait_ready(p);

        tcon.u32 = readl(SPI_TRANSMIT_CONTROL_REG(p));
        tcon.s.channel = spi->chip_select;
        tcon.s.eof = ((last_xfer && i == (p->len - 1)) ? 1 : 0);
        writel(tcon.u32, SPI_TRANSMIT_CONTROL_REG(p));

	    writel((p->tx ? p->tx[i] : 0x0), SPI_TRANSMIT_BUFFER_REG(p));

        cns3xxx_rx_wait_ready(p);

        if (p->rx)
	        p->rx[i] = readl(SPI_RECEIVE_BUFFER_REG(p));
        else
            readl(SPI_RECEIVE_BUFFER_REG(p));
	}

    spi_finalize_current_transfer(ctlr);

	return p->len;
}

static int cns3xxx_transfer_one_message(struct spi_controller *ctlr,
				    struct spi_message *msg)
{
	struct spi_transfer *xfer;
    int ret;

    msg->status = 0;

    cns3xxx_set_cs(msg->spi, true);

	list_for_each_entry(xfer, &msg->transfers, transfer_list) {
		bool last_xfer = list_is_last(&xfer->transfer_list, &msg->transfers);
		ret = cns3xxx_transfer_one(ctlr, msg->spi, xfer, last_xfer);

        if (ret < 0)
			goto out;
		
		msg->actual_length += ret;
	}

out:
	cns3xxx_set_cs(msg->spi, false);

    if (ret < 0) {
	    msg->status = ret;
        msg->actual_length = 0;
    }        

	spi_finalize_current_message(ctlr);
	return msg->status;
}

static int cns3xxx_spi_probe(struct platform_device *pdev)
{
    union cns3xxx_spi_cfg cfg;

	struct resource *mem;
	struct spi_controller *ctlr;
	struct cns3xxx_spi *p;

	int err = -ENOENT;

	ctlr = spi_alloc_master(&pdev->dev, sizeof(struct cns3xxx_spi));
	if (!ctlr)
		return -ENOMEM;

	p = spi_master_get_devdata(ctlr);
	platform_set_drvdata(pdev, ctlr);

    p->dev = &pdev->dev;

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!mem)
		return -ENODEV;

	p->base = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(p->base)) {
        dev_err(&pdev->dev, "could not map memory\n");
		err = PTR_ERR(p->base);
        p->base = 0;
		goto fail;
	}

    p->irq = platform_get_irq(pdev, 0);
	if (p->irq > -1)
		devm_request_irq(&pdev->dev, p->irq, NULL, IRQF_SHARED,
			            dev_name(&pdev->dev), ctlr);

    p->clk = devm_clk_get(p->dev, "dclk");
	if (!p->clk)
        dev_warn(&pdev->dev, "could not find dclk\n");

    if (p->clk) {
        ctlr->max_speed_hz = (clk_get_rate(p->clk) / 2);
        ctlr->min_speed_hz = (clk_get_rate(p->clk) / 128);
    }
    ctlr->transfer_one_message = cns3xxx_transfer_one_message;
	ctlr->mode_bits = SPI_CPOL | SPI_CPHA | 
                        SPI_RX_DUAL | SPI_LOOP | SPI_LSB_FIRST;
	ctlr->bits_per_word_mask = SPI_BPW_MASK(8);
	ctlr->num_chipselect = 4;
	ctlr->dev.of_node = pdev->dev.of_node;

    cfg.u32 = 0;
    cfg.s.mstrmode = 1;
    writel(cfg.u32, SPI_CONFIGURATION_REG(p));

    /* clear registers */
    writel(0x0, SPI_TRANSMIT_CONTROL_REG(p));
    writel(0x0, SPI_FIFO_TRANSMIT_CONFIG_REG(p));
    writel(0x0, SPI_FIFO_RECEIVE_CONFIG_REG(p));
    writel(0x0, SPI_INTERRUPT_ENABLE_REG(p));
    writel(0x0, SPI_INTERRUPT_STATUS_REG(p));

    cfg.s.enable = 1;
    writel(cfg.u32, SPI_CONFIGURATION_REG(p));

    err = devm_spi_register_master(&pdev->dev, ctlr);
	if (err) {
		dev_err(&pdev->dev, "register master failed: %d\n", err);
		goto fail;
	}

    dev_info(&pdev->dev, "CNS3XXX SPI bus driver\n");

	return 0;

fail:
	spi_master_put(p->ctlr);
	return err;
}

static int cns3xxx_spi_remove(struct platform_device *dev)
{ 
	struct cns3xxx_spi *p;

    p = platform_get_drvdata(dev);

    writel(0, SPI_CONFIGURATION_REG(p));

    return 0;
}

static struct platform_driver cns3xxx_spi_driver = {
	.driver		= {
		.name   = "cns3xxx_spi",
		.owner  = THIS_MODULE,
	},
	.probe		= cns3xxx_spi_probe,
	.remove		= cns3xxx_spi_remove,
};

module_platform_driver(cns3xxx_spi_driver);

MODULE_AUTHOR("Benjamin Roszak");
MODULE_DESCRIPTION("CNS3XXX SPI Controller Driver");
MODULE_LICENSE("GPL");

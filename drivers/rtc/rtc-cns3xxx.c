/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/rtc.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/io.h>

#define RTC_SEC_OFFSET_REG(p)			(u32 volatile *)(p->base + 0x00)
#define RTC_MIN_OFFSET_REG(p)			(u32 volatile *)(p->base + 0x04)
#define RTC_HOUR_OFFSET_REG(p)			(u32 volatile *)(p->base + 0x08)
#define RTC_DAY_OFFSET_REG(p)			(u32 volatile *)(p->base + 0x0C)
#define RTC_ALM_SEC_OFFSET_REG(p)		(u32 volatile *)(p->base + 0x10)
#define RTC_ALM_MIN_OFFSET_REG(p)		(u32 volatile *)(p->base + 0x14)
#define RTC_ALM_HOUR_OFFSET_REG(p)		(u32 volatile *)(p->base + 0x18)
#define RTC_REC_OFFSET_REG(p)			(u32 volatile *)(p->base + 0x1C)
#define RTC_CONFIGURATION_REG(p)		(u32 volatile *)(p->base + 0x20)
#define RTC_IRQ_STATUS_REG(p)		    (u32 volatile *)(p->base + 0x34)


#define MIN_30PPM			0x0
#define MIN_15PPM			0x1
#define MIN_10PPM			0x2
#define MIN_0PPM			0x3
#define ADD_10PPM			0x4
#define ADD_15PPM			0x5
#define ADD_30PPM			0x6


/* RTC_SEC_OFFSET_REG */
union cns3xxx_sec_offset {
	uint32_t u32;
	struct cns3xxx_sec_offset_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_16_31:16;
		uint32_t sec:16;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t sec:16;
		uint32_t reserved_16_31:16;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_MIN_OFFSET_REG */
union cns3xxx_min_offset {
	uint32_t u32;
	struct cns3xxx_min_offset_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_16_31:16;
		uint32_t min:16;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t min:16;
		uint32_t reserved_16_31:16;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_HOUR_OFFSET_REG */
union cns3xxx_hour_offset {
	uint32_t u32;
	struct cns3xxx_hour_offset_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_16_31:16;
		uint32_t hour:16;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t hour:16;
		uint32_t reserved_16_31:16;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_DAY_OFFSET_REG */
union cns3xxx_day_offset {
	uint32_t u32;
	struct cns3xxx_day_offset_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_16_31:16;
		uint32_t day:16;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t day:16;
		uint32_t reserved_16_31:16;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_ALM_SEC_OFFSET_REG */
union cns3xxx_alm_sec_offset {
	uint32_t u32;
	struct cns3xxx_alm_sec_offset_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_8_31:24;
		uint32_t sec:8;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t sec:8;
		uint32_t reserved_8_31:24;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_ALM_MIN_OFFSET_REG */
union cns3xxx_alm_min_offset {
	uint32_t u32;
	struct cns3xxx_alm_min_offset_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_8_31:24;
		uint32_t min:8;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t min:8;
		uint32_t reserved_8_31:24;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_ALM_HOUR_OFFSET_REG */
union cns3xxx_alm_hour_offset {
	uint32_t u32;
	struct cns3xxx_alm_hour_offset_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_8_31:24;
		uint32_t hour:8;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t hour:8;
		uint32_t reserved_8_31:24;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_CONFIGURATION_REG */
union cns3xxx_rtc_cfg {
	uint32_t u32;
	struct cns3xxx_rtc_cfg_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_25_31:7;
        uint32_t softreset:1;
        uint32_t digitrim:3;
        uint32_t reserved_8_20:13;
        uint32_t accesscmd:1;
        uint32_t systemclk:1;
        uint32_t matchalmirqen:1;
        uint32_t autoalmdayen:1;
        uint32_t autoalmhouren:1;
        uint32_t autoalmminen:1;
        uint32_t autoalmsecen:1;
		uint32_t enable:1;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t enable:1;
        uint32_t autoalmsecen:1;
        uint32_t autoalmminen:1;
        uint32_t autoalmhouren:1;
        uint32_t autoalmdayen:1;
        uint32_t matchalmirqen:1;
        uint32_t systemclk:1;
        uint32_t accesscmd:1;
        uint32_t reserved_8_20:13;
        uint32_t digitrim:3;
        uint32_t softreset:1;
		uint32_t reserved_25_31:7;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

/* RTC_IRQ_STATUS_REG */
union cns3xxx_irq_status {
	uint32_t u32;
	struct cns3xxx_irq_status_s {
#ifdef __BIG_ENDIAN_BITFIELD
		uint32_t reserved_5_31:27;
		uint32_t almstat:1;
		uint32_t daystat:1;
		uint32_t hourstat:1;
		uint32_t minstat:1;
		uint32_t secstat:1;
#else  /* __BIG_ENDIAN_BITFIELD */
		uint32_t secstat:1;
		uint32_t minstat:1;
		uint32_t hourstat:1;
		uint32_t daystat:1;
		uint32_t almstat:1;
		uint32_t reserved_5_31:27;
#endif /* __BIG_ENDIAN_BITFIELD */
	} s;
};

struct cns3xxx_rtc {
    struct rtc_device *dev;

    int irq;
    spinlock_t lock;
    void __iomem *base;	 
};

static int cns3xxx_alarm_irq_enable(struct device *dev, unsigned int enable)
{
    union cns3xxx_rtc_cfg cfg;

    struct cns3xxx_rtc *p;

    p = dev_get_drvdata(dev);

	spin_lock(&p->lock);

	cfg.u32 = readl(RTC_CONFIGURATION_REG(p));
    cfg.s.matchalmirqen = (enable ? 1: 0);
	writel(cfg.u32, RTC_CONFIGURATION_REG(p));

	spin_unlock(&p->lock);

	return 0;
}

static int cns3xxx_rtc_read_time(struct device *dev, struct rtc_time *rtc_tm)
{
    union cns3xxx_sec_offset sec;
    union cns3xxx_min_offset min;
    union cns3xxx_hour_offset hour;
    union cns3xxx_day_offset day;

    struct cns3xxx_rtc *p;

	unsigned long cur;
    unsigned long offset;

    p = dev_get_drvdata(dev);

	spin_lock(&p->lock);

	sec.u32  = readl(RTC_SEC_OFFSET_REG(p));
	min.u32  = readl(RTC_MIN_OFFSET_REG(p));
	hour.u32 = readl(RTC_HOUR_OFFSET_REG(p));
	day.u32  = readl(RTC_DAY_OFFSET_REG(p));
	offset   = readl(RTC_REC_OFFSET_REG(p));

	spin_unlock(&p->lock);

    cur = (((day.s.day * 24 + hour.s.hour) * 60) + 
                        min.s.min) * 60 + sec.s.sec;

	rtc_time64_to_tm(offset + cur, rtc_tm);

	return 0;
}

static int cns3xxx_rtc_set_time(struct device *dev, struct rtc_time *tm)
{
    union cns3xxx_sec_offset sec;
    union cns3xxx_min_offset min;
    union cns3xxx_hour_offset hour;
    union cns3xxx_day_offset day;

    struct cns3xxx_rtc *p;

	unsigned long cur;

    p = dev_get_drvdata(dev);

	spin_lock(&p->lock);

	sec.u32  = readl(RTC_SEC_OFFSET_REG(p));
	min.u32  = readl(RTC_MIN_OFFSET_REG(p));
	hour.u32 = readl(RTC_HOUR_OFFSET_REG(p));
	day.u32  = readl(RTC_DAY_OFFSET_REG(p));

    cur = (((day.s.day * 24 + hour.s.hour) * 60) + 
                        min.s.min) * 60 + sec.s.sec;

	writel(rtc_tm_to_time64(tm) - cur, RTC_REC_OFFSET_REG(p));

	spin_unlock(&p->lock);

	return 0;
}

static int cns3xxx_rtc_read_alarm(struct device *dev, struct rtc_wkalrm *alarm)
{
    union cns3xxx_rtc_cfg cfg;
    union cns3xxx_irq_status istat;
    union cns3xxx_alm_sec_offset sec;
    union cns3xxx_alm_min_offset min;
    union cns3xxx_alm_hour_offset hour;

    struct cns3xxx_rtc *p;
    struct rtc_time *alm;

    p = dev_get_drvdata(dev);
    alm = &alarm->time;

    cns3xxx_rtc_read_time(dev, alm);

	spin_lock(&p->lock);
	
	sec.u32  = readl(RTC_ALM_SEC_OFFSET_REG(p));
	min.u32  = readl(RTC_ALM_MIN_OFFSET_REG(p));
	hour.u32 = readl(RTC_ALM_HOUR_OFFSET_REG(p));
	
	alm->tm_sec      = sec.s.sec;
    alm->tm_min      = min.s.min;
    alm->tm_hour     = hour.s.hour;

	cfg.u32 = readl(RTC_CONFIGURATION_REG(p));
	alarm->enabled = cfg.s.matchalmirqen;

	istat.u32 = readl(RTC_IRQ_STATUS_REG(p));
	alarm->pending = istat.s.almstat;

	spin_unlock(&p->lock);

	return 0;
}

static int cns3xxx_rtc_set_alarm(struct device *dev, struct rtc_wkalrm *alarm)
{
    union cns3xxx_irq_status istat;
    union cns3xxx_alm_sec_offset sec;
    union cns3xxx_alm_min_offset min;
    union cns3xxx_alm_hour_offset hour;

    struct cns3xxx_rtc *p;
    struct rtc_time *alm;

    struct rtc_time tmp;
    unsigned long alarm_time;

    p = dev_get_drvdata(dev);
    alm = &alarm->time;

    cns3xxx_rtc_read_time(dev, &tmp);
    alarm_time = rtc_tm_to_time64(alm);

    /* alarm must be within the next 24 hours */
	if (alarm_time - rtc_tm_to_time64(&tmp) > 24 * 60 * 60)
		return -EDOM;

    /* get a rtc_time struct with alarm time in rtc clock timebase */
    rtc_time64_to_tm(alarm_time - readl(RTC_REC_OFFSET_REG(p)), &tmp);

	spin_lock(&p->lock);

    sec.s.sec   = tmp.tm_sec;
    min.s.min   = tmp.tm_min;
    hour.s.hour = tmp.tm_hour;

	writel(sec.u32, RTC_ALM_SEC_OFFSET_REG(p));
	writel(min.u32, RTC_ALM_MIN_OFFSET_REG(p));
	writel(hour.u32, RTC_ALM_HOUR_OFFSET_REG(p));

    istat.u32 = 0;
    istat.s.almstat = 1;
	writel(istat.u32, RTC_IRQ_STATUS_REG(p));

	spin_unlock(&p->lock);

    cns3xxx_alarm_irq_enable(dev, alarm->enabled);

	return 0;
}

static const struct rtc_class_ops cns3xxx_rtcops = {
	.read_time	        = cns3xxx_rtc_read_time,
	.set_time	        = cns3xxx_rtc_set_time,
	.read_alarm	        = cns3xxx_rtc_read_alarm,
	.set_alarm	        = cns3xxx_rtc_set_alarm,
    .alarm_irq_enable	= cns3xxx_alarm_irq_enable,
};

static irqreturn_t cns3xxx_rtc_irq_handler(int irq, void *data)
{
    union cns3xxx_irq_status istat;

	struct device *dev;
	struct cns3xxx_rtc *p;

    dev = data;
    p = dev_get_drvdata(dev);

    istat.u32 = 0;
    istat.s.almstat = 1;
	writel(istat.u32, RTC_IRQ_STATUS_REG(p));

    rtc_update_irq(p->dev, 1, RTC_AF | RTC_IRQF);

	return IRQ_HANDLED;
}

static int cns3xxx_rtc_probe(struct platform_device *pdev)
{
    union cns3xxx_rtc_cfg cfg;

	struct resource *mem;
	struct cns3xxx_rtc *p;

	int ret = -ENOENT;

    p = devm_kzalloc(&pdev->dev, sizeof(struct cns3xxx_rtc), GFP_KERNEL);
	if (!p) {
        dev_err(&pdev->dev, "failed to allocate cns3xxx_rtc instance\n");
		return -ENOMEM;
    }

	platform_set_drvdata(pdev, p);

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (mem == NULL) {
		dev_err(&pdev->dev, "failed to get memory region resource\n");
		return -ENODEV;
	}

    p->base = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(p->base)) {
        dev_err(&pdev->dev, "could not map memory\n");
		ret = PTR_ERR(p->base);
        p->base = 0;
		goto fail;
	}

    p->irq = platform_get_irq(pdev, 0);
	if (p->irq < 0) {
        dev_err(&pdev->dev, "failed to get irq\n");
        goto fail;
    }
	devm_request_irq(&pdev->dev, p->irq, cns3xxx_rtc_irq_handler,
                        0, dev_name(&pdev->dev), &pdev->dev);

	spin_lock_init(&p->lock);

	/* first, disable RTC and initial RTC alarm registers */
	
	writel(0, RTC_CONFIGURATION_REG(p));
	writel(0, RTC_ALM_SEC_OFFSET_REG(p));
	writel(0, RTC_ALM_MIN_OFFSET_REG(p));
	writel(0, RTC_ALM_HOUR_OFFSET_REG(p));

    cfg.u32 = readl(RTC_CONFIGURATION_REG(p));
    cfg.s.digitrim = MIN_0PPM;
    cfg.s.accesscmd = 1;
    cfg.s.enable = 1;
	writel(cfg.u32, RTC_CONFIGURATION_REG(p));

    p->dev = devm_rtc_device_register(&pdev->dev, "cns3xxx-rtc", 
                                        &cns3xxx_rtcops, THIS_MODULE);
	if (IS_ERR(p->dev)) {
		dev_err(&pdev->dev, "cannot attach rtc\n");
		ret = PTR_ERR(p->dev);
		goto fail;
	}

	return 0;

fail:
	return ret;
}

static int cns3xxx_rtc_remove(struct platform_device *dev)
{
	platform_set_drvdata(dev, NULL);

	return 0;
}

static struct platform_driver cns3xxx_rtcdrv = {
	.driver		= {
	    .name 	= "cns3xxx-rtc",
	    .owner 	= THIS_MODULE,
	},
	.probe		= cns3xxx_rtc_probe,
    .remove		= cns3xxx_rtc_remove,
};

module_platform_driver(cns3xxx_rtcdrv);

MODULE_AUTHOR("Benjamin Roszak");
MODULE_DESCRIPTION("CNS3XXX RTC Driver");
MODULE_LICENSE("GPL");
